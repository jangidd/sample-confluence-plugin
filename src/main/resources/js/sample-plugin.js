AJS.toInit(function ($) {
    $(".dynamic-macro").submit(function (eve) {
        eve.preventDefault();
        var term = $(this).find(".text").val();
        var root = $(this).parents(".search-results");
        var resultsContainer = root.find(".results");
        var totalContainer = root.find(".total");

        if (term) {
            // API REFERENCE - https://docs.atlassian.com/ConfluenceServer/rest/7.2.0/#api/content-search
            $.getJSON(contextPath + '/rest/api/content/search?cql=siteSearch ~ ' + term, function (data) {
                  totalContainer.html(data.size);
                  var items = [];
                  $.each(data.results, function(k, v) {
                    items.push( "<li>" + v.title + "</li>" );
                  });
                  $( "<ul/>", {
                    html: items.join( "" )
                  }).appendTo(resultsContainer);
            });
        }
        return false;
    });
});