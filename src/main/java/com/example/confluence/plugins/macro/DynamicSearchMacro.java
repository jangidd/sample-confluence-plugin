package com.example.confluence.plugins.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;

import java.util.Map;

/**
 * DPKJ
 * 12/12/19
 */
@Scanned
public class DynamicSearchMacro implements Macro
{
  private static final String TEMPLATE = "templates/dynamic-search-macro.vm";

  @Override
  public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException
  {
    Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
    return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
  }

  @Override
  public BodyType getBodyType()
  {
    return BodyType.NONE;
  }

  @Override
  public OutputType getOutputType()
  {
    return OutputType.BLOCK;
  }
}
