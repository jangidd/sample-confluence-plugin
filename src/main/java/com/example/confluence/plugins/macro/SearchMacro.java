package com.example.confluence.plugins.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.v2.*;
import com.atlassian.confluence.search.v2.query.TextQuery;
import com.atlassian.confluence.search.v2.searchfilter.SiteSearchPermissionsSearchFilter;
import com.atlassian.confluence.search.v2.sort.ModifiedSort;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * DPKJ
 * 12/12/19
 */
@Scanned
public class SearchMacro implements Macro
{
  private static final String TEMPLATE = "templates/search-macro.vm";

  private SearchManager searchManager;

  @Autowired
  public SearchMacro(
      @ComponentImport SearchManager searchManager
  )
  {
    this.searchManager = searchManager;
  }

  @Override
  public String execute(Map<String, String> params, String body, ConversionContext conversionContext) throws MacroExecutionException
  {
    String search = params.get("search"); //getting macro search text input

    Map<String, Object> contextMap = MacroUtils.defaultVelocityContext();
    contextMap.put("term", search);
    search(search, contextMap);
    return VelocityUtils.getRenderedTemplate(TEMPLATE, contextMap);
  }

  @Override
  public BodyType getBodyType()
  {
    return BodyType.NONE;
  }

  @Override
  public OutputType getOutputType()
  {
    return OutputType.BLOCK;
  }

  private void search(String queryString, Map<String, Object> contextMap)
  {
    SearchQuery query = new TextQuery(queryString);
    SearchSort sort = new ModifiedSort(SearchSort.Order.DESCENDING); // latest modified content first
    SearchFilter searchFilter = SiteSearchPermissionsSearchFilter.getInstance();
    ContentSearch search = new ContentSearch(query, sort, searchFilter, 0, 10);
    SearchResults searchResults;
    try {
      searchResults = searchManager.search(search);
    } catch (InvalidSearchException e) { // discard search and assign empty results
      searchResults = new DefaultSearchResults(Collections.emptyList(), 0);
    }

    contextMap.put("total", searchResults.getUnfilteredResultsCount());
    List<String> results = new ArrayList<String>();
    for (SearchResult searchResult : searchResults.getAll()) {
      results.add(searchResult.getDisplayTitle());
      /**
       * You can use any of these if you need more details in results
       System.out.println("Title: " + searchResult.getDisplayTitle());
       System.out.println("Content: " + searchResult.getContent());
       System.out.println("SpaceKey: " + searchResult.getSpaceKey());
       */
    }

    contextMap.put("results", results);
  }
}
