package com.example.confluence.plugins.api;

public interface MyPluginComponent
{
    String getName();
}