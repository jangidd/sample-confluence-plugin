package ut.com.example.confluence.plugins;

import org.junit.Test;
import com.example.confluence.plugins.api.MyPluginComponent;
import com.example.confluence.plugins.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}